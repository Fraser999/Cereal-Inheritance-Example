#include <cassert>
#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "derived_types.hpp"
#include "routing/routing.hpp"

int main() {
  using Base = routing::AccountBase;
  using Account1 = maidsafe::Account1;
  using Account2 = maidsafe::Account2;
  using BasePtr = std::shared_ptr<Base> ;

  BasePtr account1A(std::make_shared<Account1>("First Type 1 Account"));
  BasePtr account1B(std::make_shared<Account1>("Second Type 1 Account"));
  BasePtr account2A(std::make_shared<Account2>("First Type 2 Account"));
  BasePtr account2B(std::make_shared<Account2>("Second Type 2 Account"));

  routing::Sentinel sentinel;
  BasePtr merged_result;

  // Add one of each type - none should merge
  merged_result = sentinel.AddAccountTransferResult(account1A);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account1B);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account2A);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account2B);
  assert(!merged_result);

  // Add another one of each type - the Account1 types should merge now
  merged_result = sentinel.AddAccountTransferResult(account1A);
  assert(merged_result);
  assert(merged_result->name_and_type.first == "First Type 1 Account");
  assert(merged_result->name_and_type.second == 1);
  assert(std::dynamic_pointer_cast<Account1>(merged_result)->data_A == 111);

  merged_result = sentinel.AddAccountTransferResult(account1B);
  assert(merged_result);
  assert(merged_result->name_and_type.first == "Second Type 1 Account");
  assert(merged_result->name_and_type.second == 1);
  assert(std::dynamic_pointer_cast<Account1>(merged_result)->data_A == 111);

  merged_result = sentinel.AddAccountTransferResult(account2A);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account2B);
  assert(!merged_result);

  // Add another one of each type - the Account2 types should merge now
  merged_result = sentinel.AddAccountTransferResult(account1A);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account1B);
  assert(!merged_result);

  merged_result = sentinel.AddAccountTransferResult(account2A);
  assert(merged_result);
  assert(merged_result->name_and_type.first == "First Type 2 Account");
  assert(merged_result->name_and_type.second == 2);
  assert(std::dynamic_pointer_cast<Account2>(merged_result)->data_A == 222111);
  assert(std::dynamic_pointer_cast<Account2>(merged_result)->data_B == 222222);

  merged_result = sentinel.AddAccountTransferResult(account2B);
  assert(merged_result);
  assert(merged_result->name_and_type.first == "Second Type 2 Account");
  assert(merged_result->name_and_type.second == 2);
  assert(std::dynamic_pointer_cast<Account2>(merged_result)->data_A == 222111);
  assert(std::dynamic_pointer_cast<Account2>(merged_result)->data_B == 222222);

  return 0;
}

#if 0
int main() {
  using Base = routing::Base;
  using PublicPmid = maidsafe::PublicPmid;
  using ImmutableData = third_party::ImmutableData;

  std::string serialised_public_pmid, serialised_immutable_data;
  {
    PublicPmid* public_pmid = new PublicPmid;
    public_pmid->base_data = "'base_data' in a PublicPmid instance.\n";
    public_pmid->data = "'data' in a PublicPmid instance.\n";
    std::unique_ptr<Base> public_pmid_as_base(dynamic_cast<Base*>(public_pmid));
    serialised_public_pmid = routing::Dispatch(public_pmid_as_base);

    ImmutableData* immutable_data = new ImmutableData;
    immutable_data->base_data = "'base_data' in an ImmutableData instance.\n";
    immutable_data->data = "'data' in an ImmutableData instance.\n";
    std::unique_ptr<Base> immutable_data_as_base(dynamic_cast<Base*>(immutable_data));
    serialised_immutable_data = routing::Dispatch(immutable_data_as_base);
  }

  {
    std::unique_ptr<Base> public_pmid_as_base(routing::Service(std::move(serialised_public_pmid)));
    PublicPmid& public_pmid(dynamic_cast<PublicPmid&>(*public_pmid_as_base));

    std::unique_ptr<Base> immutable_data_as_base(routing::Service(std::move(serialised_immutable_data)));
    ImmutableData& immutable_data(dynamic_cast<ImmutableData&>(*immutable_data_as_base));

    std::cout << public_pmid.base_data << public_pmid.data << '\n'
              << immutable_data.base_data << immutable_data.data << '\n';
  }

  //for (size_t i = 0; i < serialised_public_pmid.size(); ++i) {
  //  std::cout << "assert(static_cast<std::uint8_t>(serialised_public_pmid[" << i << "]) == "
  //            << unsigned(std::uint8_t(serialised_public_pmid[i]))<< ");\n";
  //}

  assert(static_cast<std::uint8_t>(serialised_public_pmid[0]) == 1);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[1]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[2]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[3]) == 128);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[4]) == 20);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[5]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[6]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[7]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[8]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[9]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[10]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[11]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[12]) == 109);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[13]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[14]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[15]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[16]) == 115);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[17]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[18]) == 102);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[19]) == 101);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[20]) == 58);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[21]) == 58);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[22]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[23]) == 117);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[24]) == 98);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[25]) == 108);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[26]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[27]) == 99);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[28]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[29]) == 109);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[30]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[31]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[32]) == 1);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[33]) == 38);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[34]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[35]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[36]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[37]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[38]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[39]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[40]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[41]) == 39);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[42]) == 98);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[43]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[44]) == 115);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[45]) == 101);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[46]) == 95);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[47]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[48]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[49]) == 116);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[50]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[51]) == 39);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[52]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[53]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[54]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[55]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[56]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[57]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[58]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[59]) == 117);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[60]) == 98);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[61]) == 108);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[62]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[63]) == 99);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[64]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[65]) == 109);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[66]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[67]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[68]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[69]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[70]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[71]) == 115);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[72]) == 116);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[73]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[74]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[75]) == 99);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[76]) == 101);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[77]) == 46);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[78]) == 10);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[79]) == 33);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[80]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[81]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[82]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[83]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[84]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[85]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[86]) == 0);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[87]) == 39);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[88]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[89]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[90]) == 116);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[91]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[92]) == 39);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[93]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[94]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[95]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[96]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[97]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[98]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[99]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[100]) == 117);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[101]) == 98);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[102]) == 108);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[103]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[104]) == 99);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[105]) == 80);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[106]) == 109);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[107]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[108]) == 100);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[109]) == 32);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[110]) == 105);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[111]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[112]) == 115);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[113]) == 116);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[114]) == 97);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[115]) == 110);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[116]) == 99);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[117]) == 101);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[118]) == 46);
  assert(static_cast<std::uint8_t>(serialised_public_pmid[119]) == 10);
  return 0;
}
#endif
