#ifndef DERIVED_TYPES_HPP
#define DERIVED_TYPES_HPP

#include <memory>
#include <string>

#include "cereal/archives/binary.hpp"
#include "cereal/types/base_class.hpp"
#include "cereal/types/polymorphic.hpp"
#include "cereal/types/string.hpp"

#include "routing/base.hpp"

namespace maidsafe {

struct PublicPmid : public routing::Base {
  PublicPmid();
  ~PublicPmid() final;
  template <typename Archive>
  void serialize(Archive &archive) { archive(cereal::base_class<Base>(this), data); }
  std::string data;
};



// Account with one data member
struct Account1 : public routing::AccountBase, public std::enable_shared_from_this<Account1> {
  using BasePtr = std::shared_ptr<routing::AccountBase>;

  explicit Account1(std::string name_in) : routing::AccountBase(std::move(name_in), 1), data_A(1) {}
  Account1() = default;
  ~Account1() final = default;

  template <typename Archive>
  void serialize(Archive &archive) { archive(cereal::base_class<routing::AccountBase>(this), data_A); }

  virtual BasePtr Merge(std::vector<BasePtr>& accumulated) override;

  int data_A;
};



// Account with two data members
struct Account2 : public routing::AccountBase, public std::enable_shared_from_this<Account2> {
  using BasePtr = std::shared_ptr<routing::AccountBase>;

  explicit Account2(std::string name_in) : routing::AccountBase(std::move(name_in), 2), data_A(21), data_B(22) {}
  Account2() = default;
  ~Account2() final = default;

  template <typename Archive>
  void serialize(Archive &archive) { archive(cereal::base_class<routing::AccountBase>(this), data_A, data_B); }

  virtual BasePtr Merge(std::vector<BasePtr>& accumulated) override;

  int data_A, data_B;
};

}  // namespace maidsafe



namespace third_party {

struct ImmutableData : public routing::Base {
  ImmutableData();
  ~ImmutableData() final;
  template <typename Archive>
  //void serialize(Archive &archive) { archive(cereal::base_class<Base>(this), data); }
  //template <typename Archive>
  void save(Archive &archive) const { archive(cereal::base_class<Base>(this), data); }
  template <typename Archive>
  void load(Archive &archive) { archive(cereal::base_class<Base>(this), data); }
  std::string data;
};

}  // namespace third_party

CEREAL_REGISTER_TYPE(maidsafe::PublicPmid);
CEREAL_REGISTER_TYPE(third_party::ImmutableData);

#endif  // DERIVED_TYPES_HPP
