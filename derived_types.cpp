#include "derived_types.hpp"

#include <cassert>

namespace maidsafe {

PublicPmid::PublicPmid() : routing::Base(), data("Default initialised PublicPmid.\n") {}

PublicPmid::~PublicPmid() {}

Account1::BasePtr Account1::Merge(std::vector<BasePtr>& accumulated) {
  // Add this account to the accumulated collection
  accumulated.push_back(std::dynamic_pointer_cast<routing::AccountBase>(shared_from_this()));
  std::shared_ptr<Account1> result;
  // For this account type, the merge will be completed once we have 2 results
  if (accumulated.size() == 2u) {
    assert(std::dynamic_pointer_cast<Account1>(accumulated[0])->data_A == 1);
    // Prepare the merged result with whatever data we want from the base and derived class
    result = shared_from_this();
    result->data_A = 111;
  }
  return result;
}

Account2::BasePtr Account2::Merge(std::vector<BasePtr>& accumulated) {
  // Add this account to the accumulated collection
  accumulated.push_back(std::dynamic_pointer_cast<routing::AccountBase>(shared_from_this()));
  std::shared_ptr<Account2> result;
  // For this account type, the merge will be completed once we have 3 results
  if (accumulated.size() == 3u) {
    assert(std::dynamic_pointer_cast<Account2>(accumulated[0])->data_A == 21);
    assert(std::dynamic_pointer_cast<Account2>(accumulated[0])->data_B == 22);
    // Prepare the merged result with whatever data we want from the base and derived class
    result = shared_from_this();
    result->data_A = 222111;
    result->data_B = 222222;
  }
  return result;
}

}  // namespace maidsafe



namespace third_party {

ImmutableData::ImmutableData() : routing::Base(), data("Default initialised ImmutableData.\n") {}

ImmutableData::~ImmutableData() {}

}  // namespace third_party
