#ifndef ROUTING_BASE_HPP
#define ROUTING_BASE_HPP

#include <memory>
#include <utility>
#include <vector>
#include "cereal/types/string.hpp"
#include "cereal/types/utility.hpp"

namespace routing {

struct Base {
  std::string base_data = "Default initialised Base.\n";
  virtual ~Base() = default;
  template <typename Archive>
  void serialize(Archive &archive) { archive(base_data); }
};

struct AccountBase {
  AccountBase(std::string name_in, int type_in) : name_and_type(std::make_pair(std::move(name_in), type_in)) {}
  AccountBase() = default;
  virtual ~AccountBase() = default;
  template <typename Archive>
  void serialize(Archive &archive) { archive(name_and_type); }
  virtual std::shared_ptr<AccountBase> Merge(std::vector<std::shared_ptr<AccountBase>>& accumulated) = 0;

  std::pair<std::string, int> name_and_type;
};

}  // namespace routing

#endif  // ROUTING_BASE_HPP
