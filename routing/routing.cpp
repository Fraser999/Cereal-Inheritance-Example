#include "routing.hpp"

#include <sstream>
#include <vector>

#include "cereal/archives/binary.hpp"
#include "cereal/types/polymorphic.hpp"

#include "base.hpp"

namespace routing {

std::string Dispatch(const std::unique_ptr<Base>& base) {
  std::stringstream ss;
  cereal::BinaryOutputArchive oarchive(ss);
  oarchive(base);
  return ss.str();
}

std::unique_ptr<Base> Service(std::string&& serialised) {
  std::stringstream ss(std::move(serialised));
  cereal::BinaryInputArchive iarchive(ss);
  std::unique_ptr<Base> base;
  iarchive(base);
  return base;
}

std::shared_ptr<AccountBase> Sentinel::AddAccountTransferResult(std::shared_ptr<AccountBase> account) {
  std::shared_ptr<AccountBase> result;
  // Find the account
  auto itr = pendings.find(account->name_and_type);
  if (itr == pendings.end())  // Insert a new entry
    pendings.emplace(account->name_and_type, std::vector<std::shared_ptr<AccountBase>>(1, account));
  else  // Try to merge
    result = account->Merge(itr->second);

  // If the merge succeeded, erase the entry.
  if (result)
    pendings.erase(itr);

  return result;
}

}  // namespace routing
