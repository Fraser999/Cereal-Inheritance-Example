#ifndef ROUTING_ROUTING_HPP
#define ROUTING_ROUTING_HPP

#include <memory>
#include <map>
#include <string>
#include <vector>

namespace routing {

struct Base;
struct AccountBase;

std::string Dispatch(const std::unique_ptr<Base>& base);

std::unique_ptr<Base> Service(std::string&& serialised);

class Sentinel {
 public:
  std::shared_ptr<AccountBase> AddAccountTransferResult(std::shared_ptr<AccountBase> account);

 private:
  std::map<std::pair<std::string, int>, std::vector<std::shared_ptr<AccountBase>>> pendings;
};

}  // namespace routing

#endif  // ROUTING_ROUTING_HPP
